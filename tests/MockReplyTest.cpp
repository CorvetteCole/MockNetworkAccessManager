#include "MockNetworkAccessManager.hpp"

#include "QSignalInspector.hpp"
#include "TestUtils.hpp"

#include <QtTest>
#include <QtDebug>
#include <QSharedPointer>

namespace Tests {

/*! Implements unit tests for the MockReply and MockReplyBuilder classes.
 */
class MockReplyTest : public QObject
{
	Q_OBJECT

private slots:
	void testRequestVerb();
	void testRequestVerb_data();
	void testCopy();
	void testCopy_data();
	void testCloneFinishedReply();
	void testIsRedirectToBeFollowed();
	void testIsRedirectToBeFollowed_data();
	void testPrepareRemoveFileLocalhost();
	void testPrepare();
	void testPrepare_data();
	void testDefaultErrorString();
	void testDefaultErrorString_data();
	void testFinish();
	void testFinish_data();
	void testAbort();

	void testNullBuilder();
	void testWithBodyByteArray_data();
	void testWithBodyByteArray();
	void testWithBodyJson_data();
	void testWithBodyJson();
	void testWithFile();
	void testWithFile_data();
	void testWithStatus_data();
	void testWithStatus();
	void testWithError_data();
	void testWithError();
	void testWithRedirect();
	void testWithRedirect_data();
	void testWithHeader();
	void testWithHeader_data();
	void testWithRawHeader_data();
	void testWithRawHeader();
	void testWithAttribute_data();
	void testWithAttribute();
	void testWithAuthenticate();
	void testWithAuthenticate_data();
	void testWithCookie_data();
	void testWithCookie();
	void testWithFinishDelayUntil();
	void testWithFinishDelayUntilMemberPointer();
	void testEqualityOperators();
	void testEqualityOperators_data();

	void testErrorAndStatusCodeConsistencyWarning();
	void testErrorAndStatusCodeConsistencyWarning_data();

};


using namespace MockNetworkAccess;


//####### Helpers #######

typedef QHash<QNetworkRequest::Attribute, QVariant> AttributeHash;
typedef QSharedPointer<MockReplyBuilder> MockReplyBuilderPtr;
typedef std::unique_ptr<MockReply> MockReplyUniquePtr;
typedef QPair<bool, QRegularExpression> OptionalRegEx;



class MockReplyTestHelper : public MockReply
{
	Q_OBJECT

public:
	/* Hack to access protected methods of base class: https://stackoverflow.com/a/23904680/490560
	 * For this to work, MockReplyTestHelper may not implement methods with the same name
	 * as the protected methods of MockReply we want to call.
	 * Therefore, we use a "call" prefix.
	 */

	static void callPrepare(MockReply* reply, const Request& req)
	{
		(reply->*&MockReplyTestHelper::prepare)(req);
	}

	static void callFinish(MockReply* reply, const Request& req)
	{
		(reply->*&MockReplyTestHelper::finish)(req);
	}

	static void callSetBehaviorFlags(MockReply* reply, BehaviorFlags flags)
	{
		(reply->*&MockReplyTestHelper::setBehaviorFlags)(flags);
	}

};

class DummyObject : public QObject
{
	Q_OBJECT

public:
	DummyObject( QObject* parent = Q_NULLPTR )
		: QObject( parent )
	{}

Q_SIGNALS:
	void dummySignal();
};


const QString contentTypeTextPlain( "text/plain" );
const QString contentTypeOctetstream( "application/octet-stream" );
const QString contentTypeJson( "application/json" );
const QByteArray customHeader( "X-Custom-Header" );

const QUrl fileUrl( "file:///path/to/file.txt" );
const Request fileGetRequest( QNetworkRequest( fileUrl ), QNetworkAccessManager::GetOperation );
const Request fileHeadRequest( QNetworkRequest( fileUrl ), QNetworkAccessManager::HeadOperation );
const Request filePutRequest( QNetworkRequest( fileUrl ), QNetworkAccessManager::PutOperation, QByteArray( "{\"foo\":\"bar\"}" ) );

const QUrl qrcUrl( "qrc:/path/to/file.txt" );
const Request qrcGetRequest( QNetworkRequest( qrcUrl ), QNetworkAccessManager::GetOperation );
const Request qrcHeadRequest( QNetworkRequest( qrcUrl ), QNetworkAccessManager::HeadOperation );
const Request qrcPutRequest( QNetworkRequest( qrcUrl ), QNetworkAccessManager::PutOperation, QByteArray( "{\"foo\":\"bar\"}" ) );



namespace HelperMethods
{
	MockReplyBuilderPtr createFullBuilder();
	MockReplyBuilderPtr createErrorBuilder();
	MockReplyBuilder createErrorBuilder( QNetworkReply::NetworkError error );
	HeaderHash getKnownHeaders(QNetworkReply* reply);
};

OptionalRegEx makeOptionalRegEx( const QString& pattern,
                                 QRegularExpression::PatternOptions options = QRegularExpression::NoPatternOption );

} // namespace Tests


Q_DECLARE_METATYPE( MockNetworkAccess::BehaviorFlags )
Q_DECLARE_METATYPE( MockNetworkAccess::HeaderHash )
Q_DECLARE_METATYPE( Tests::MockReplyBuilderPtr )
Q_DECLARE_METATYPE( QNetworkRequest::KnownHeaders )
Q_DECLARE_METATYPE( QNetworkRequest::Attribute )
Q_DECLARE_METATYPE( MockNetworkAccess::HttpUtils::Authentication::Challenge::Ptr )
Q_DECLARE_METATYPE( Tests::OptionalRegEx )


namespace Tests
{

//####### Tests #######


/*! \test Tests the Request::verb() method.
 */
void MockReplyTest::testRequestVerb()
{
	QFETCH( Request, request );

	QTEST( request.verb(), "expectedVerb" );
}

/*! Provides the data for the testRequestVerb() test.
 */
void MockReplyTest::testRequestVerb_data()
{
	QTest::addColumn<Request>( "request" );
	QTest::addColumn<QString>( "expectedVerb" );

	const QUrl dummyUrl( "http://example.com" );
	const QNetworkRequest dummyQRequest( dummyUrl );
	QNetworkRequest customVerbRequest( dummyUrl );
	customVerbRequest.setAttribute( QNetworkRequest::CustomVerbAttribute, "foo" );

	//                         // request                                                               // expectedVerb
	QTest::newRow( "GET" )     << Request( QNetworkAccessManager::GetOperation, dummyQRequest )         << "GET";
	QTest::newRow( "POST" )    << Request( QNetworkAccessManager::PostOperation, dummyQRequest )        << "POST";
	QTest::newRow( "PUT" )     << Request( QNetworkAccessManager::PutOperation, dummyQRequest )         << "PUT";
	QTest::newRow( "DELETE" )  << Request( QNetworkAccessManager::DeleteOperation, dummyQRequest )      << "DELETE";
	QTest::newRow( "HEAD" )    << Request( QNetworkAccessManager::HeadOperation, dummyQRequest )        << "HEAD";
	QTest::newRow( "Custom" )  << Request( QNetworkAccessManager::CustomOperation, customVerbRequest  ) << "foo";
}


/*! \test Tests the methods to copy MockReplyBuilder objects.
 * Implicitly also tests the MockReply::clone() method.
 */
void MockReplyTest::testCopy()
{
	QFETCH(MockReplyBuilderPtr, original);
	QFETCH(MockReplyBuilderPtr, copy);

	MockReplyUniquePtr originalReply(original->createReply());
	MockReplyUniquePtr copyReply(copy->createReply());

	if (!originalReply)
		QVERIFY(!copyReply);
	else
	{
		QCOMPARE(copyReply->isFinished(), originalReply->isFinished());
		QCOMPARE(copyReply->isOpen(), originalReply->isOpen());
		QCOMPARE(copyReply->body(), originalReply->body());
		QCOMPARE(copyReply->bytesAvailable(), originalReply->bytesAvailable());
		QCOMPARE(copyReply->size(), originalReply->size());
		QCOMPARE(copyReply->attributes(), originalReply->attributes());
		QCOMPARE(copyReply->rawHeaderPairs(), originalReply->rawHeaderPairs());
		QCOMPARE(HelperMethods::getKnownHeaders(copyReply.get()), HelperMethods::getKnownHeaders(originalReply.get()));
		QCOMPARE(copyReply->error(), originalReply->error());
	}
}

/*! Provides the data for the testCopy() test.
 */
void MockReplyTest::testCopy_data()
{
	QTest::addColumn< MockReplyBuilderPtr >( "original" );
	QTest::addColumn< MockReplyBuilderPtr >( "copy" );

	MockReplyBuilderPtr original;
	MockReplyBuilderPtr copy;

	// Unconfigured builder
	original.reset( new MockReplyBuilder() );
	copy.reset( new MockReplyBuilder( *original ) );
	QTest::newRow( "unconfigured; copy constructor" ) << original << copy;

	copy.reset( new MockReplyBuilder( MockReplyBuilder( *original ) ) );
	QTest::newRow( "unconfigured; move constructor" ) << original << copy;

	copy.reset( new MockReplyBuilder() );
	*copy = *original;
	QTest::newRow( "unconfigured; copy operator" ) << original << copy;

	copy.reset( new MockReplyBuilder() );
	*copy = MockReplyBuilder( *original );
	QTest::newRow( "unconfigured; move operator" ) << original << copy;

	copy.reset( new MockReplyBuilder() );
	copy->with( *original );
	QTest::newRow( "unconfigured; with method" ) << original << copy;

	copy.reset( new MockReplyBuilder() );
	copy->with( MockReplyBuilder( *original ) );
	QTest::newRow( "unconfigured; with move method" ) << original << copy;

	// Successful replies
	original.reset( new MockReplyBuilder() );
	original->withStatus( HttpStatus::OK );
	original->withBody( "foo bar" );
	original->withHeader( QNetworkRequest::ContentTypeHeader, QString( "text/plain" ) );
	original->withAttribute( QNetworkRequest::HttpStatusCodeAttribute, 200 );
	copy.reset( new MockReplyBuilder( *original ) );
	QTest::newRow( "successful replies; copy constructor" ) << original << copy;

	copy.reset( new MockReplyBuilder( MockReplyBuilder( *original ) ) );
	QTest::newRow( "successful replies; move constructor" ) << original << copy;

	copy.reset( new MockReplyBuilder() );
	*copy = *original;
	QTest::newRow( "successful replies; copy operator" ) << original << copy;

	copy.reset( new MockReplyBuilder() );
	*copy = MockReplyBuilder( *original );
	QTest::newRow( "successful replies; move operator" ) << original << copy;

	copy.reset( new MockReplyBuilder() );
	copy->with( *original );
	QTest::newRow( "successful replies; with method" ) << original << copy;

	copy.reset( new MockReplyBuilder() );
	copy->with( MockReplyBuilder( *original ) );
	QTest::newRow( "successful replies; with move method" ) << original << copy;
}

/*! \test Tests MockReply::clone() for finished MockReply objects.
 */
void MockReplyTest::testCloneFinishedReply()
{
	MockReplyBuilderPtr builder(new MockReplyBuilder());
	builder->withStatus(HttpStatus::OK);
	builder->withBody("foo bar");

	MockReplyUniquePtr reply(builder->createReply());

	Request request(QNetworkRequest(QUrl("http://example.com")));
	MockReplyTestHelper::callFinish(reply.get(), request);

	MockReplyUniquePtr clone(reply->clone());

	QCOMPARE(clone->isFinished(), reply->isFinished());
	QCOMPARE(clone->isOpen(), reply->isOpen());
	QCOMPARE(clone->body(), reply->body());
	QCOMPARE(clone->bytesAvailable(), reply->bytesAvailable());
	QCOMPARE(clone->size(), reply->size());
	QCOMPARE(clone->attributes(), reply->attributes());
	QCOMPARE(clone->rawHeaderPairs(), reply->rawHeaderPairs());
	QCOMPARE(HelperMethods::getKnownHeaders(clone.get()), HelperMethods::getKnownHeaders(reply.get()));
	QCOMPARE(clone->error(), reply->error());

}

/*! \test Tests MockReply::isRedirectToBeFollowed().
 */
void MockReplyTest::testIsRedirectToBeFollowed()
{
	QFETCH( MockReplyBuilder, builder );
	QFETCH( BehaviorFlags, behaviorFlags );

	MockReplyUniquePtr reply( builder.createReply() );

	MockReplyTestHelper::callSetBehaviorFlags( reply.get(), behaviorFlags );

	QTEST( reply->isRedirectToBeFollowed(), "isRedirectToBeFollowed" );

}

/*! Provides the data for the MockReplyTest::testIsRedirectToBeFollowed() test.
 */
void MockReplyTest::testIsRedirectToBeFollowed_data()
{
	QTest::addColumn<MockReplyBuilder>( "builder" );
	QTest::addColumn<BehaviorFlags>( "behaviorFlags" );
	QTest::addColumn<bool>( "isRedirectToBeFollowed" );

	const QUrl exampleRootUrl("http://example.com");

	MockReplyBuilder noStatusCodeBuilder;
	noStatusCodeBuilder.withBody("foo");
	MockReplyBuilder invalidLocationBuilder;
	invalidLocationBuilder.withRedirect(QUrl());
	MockReplyBuilder relativeRedirectBuilder;
	relativeRedirectBuilder.withRedirect(QUrl("foo/bar"));
	MockReplyBuilder protRelRedirectBuilder;
	protRelRedirectBuilder.withRedirect(QUrl("//example.com/foo"));
	MockReplyBuilder redirect302Builder;
	redirect302Builder.withRedirect(exampleRootUrl, HttpStatus::Found);
	MockReplyBuilder redirect307Builder;
	redirect307Builder.withRedirect(exampleRootUrl, HttpStatus::TemporaryRedirect);
	MockReplyBuilder redirect308Builder;
	redirect308Builder.withRedirect(exampleRootUrl, HttpStatus::PermanentRedirect);

	const BehaviorFlags qt_5_6_Behavior = Behavior_Qt_5_6_0;
	const BehaviorFlags expectedBehavior = Behavior_Expected;


	//                                                       // builder                 //behaviorFlags     // isRedirectToBeFollowed
	QTest::newRow( "no status code" )                        << noStatusCodeBuilder     << qt_5_6_Behavior  << false;
	QTest::newRow( "invalid location header" )               << invalidLocationBuilder  << qt_5_6_Behavior  << false;
	QTest::newRow( "relative redirect (Qt 5.6)" )            << relativeRedirectBuilder << qt_5_6_Behavior  << true;
	QTest::newRow( "relative redirect (expected)" )          << relativeRedirectBuilder << expectedBehavior << true;
	QTest::newRow( "protocol relative redirect (Qt 5.6)" )   << protRelRedirectBuilder  << qt_5_6_Behavior  << true;
	QTest::newRow( "protocol relative redirect (expected)" ) << protRelRedirectBuilder  << expectedBehavior << true;
	QTest::newRow( "redirect 302" )                          << redirect302Builder      << qt_5_6_Behavior  << true;
	QTest::newRow( "redirect 307" )                          << redirect307Builder      << qt_5_6_Behavior  << true;
	QTest::newRow( "redirect 308 (Qt 5.6)" )                 << redirect308Builder      << qt_5_6_Behavior  << false;
	QTest::newRow( "redirect 308 (expected)" )               << redirect308Builder      << expectedBehavior << true;
}


/*! \test Tests that the host "localhost" is removed from reply URLs for file:// request.
 */
void MockReplyTest::testPrepareRemoveFileLocalhost()
{
	Request request( QNetworkRequest( QUrl( "file://localhost/path/to/file" ) ), QNetworkAccessManager::GetOperation );

	MockReplyUniquePtr reply( MockReplyBuilder().withError( QNetworkReply::NoError ).createReply() );

	MockReplyTestHelper::callPrepare( reply.get(), request );

	QVERIFY( reply->url().host().isEmpty() );
}


/*! \test Tests the MockReply::prepare() method.
 */
void MockReplyTest::testPrepare()
{
	QFETCH( BehaviorFlags, behaviorFlags );
	QFETCH( MockReplyBuilder, builder );
	QFETCH( Request, request );
	QFETCH( AttributeHash, expectedAttributes );
	QFETCH( RawHeaderHash, expectedRawHeaders );
	QFETCH( HeaderHash, expectedHeaders );
	QFETCH( OptionalRegEx, expectedWarning );

	MockReplyUniquePtr reply( builder.createReply() );

	MockReplyTestHelper::callSetBehaviorFlags( reply.get(), behaviorFlags );

	if( expectedWarning.first )
		Tests::ignoreMessage( QtWarningMsg, expectedWarning.second );
	MockReplyTestHelper::callPrepare( reply.get(), request );

	QVERIFY( !reply->isFinished() );
	QVERIFY( !reply->isReadable() );
	QVERIFY( !reply->isOpen() );
	QCOMPARE( reply->request(), request.qRequest );
	QTEST( reply->error(), "expectedError" );
	QTEST( reply->errorString(), "expectedErrorString" );
	QCOMPARE( reply->url(), request.qRequest.url() );
	for ( AttributeHash::const_iterator iter = expectedAttributes.cbegin(); iter != expectedAttributes.cend(); ++iter )
		QCOMPARE( reply->attribute( iter.key() ), iter.value() );

	for ( RawHeaderHash::const_iterator iter = expectedRawHeaders.cbegin(); iter != expectedRawHeaders.cend(); ++iter )
		QCOMPARE( reply->rawHeader( iter.key() ), iter.value() );

	for ( HeaderHash::const_iterator iter = expectedHeaders.cbegin(); iter != expectedHeaders.cend(); ++iter )
		QCOMPARE( reply->header( iter.key() ), iter.value() );
}

/*! Provides the data for the testPrepare() test.
 */
void MockReplyTest::testPrepare_data()
{
	QTest::addColumn< MockReplyBuilder >( "builder" );
	QTest::addColumn< BehaviorFlags >( "behaviorFlags" );
	QTest::addColumn< Request >( "request" );
	QTest::addColumn< AttributeHash >( "expectedAttributes" );
	QTest::addColumn< RawHeaderHash >( "expectedRawHeaders" );
	QTest::addColumn< HeaderHash >( "expectedHeaders" );
	QTest::addColumn< QNetworkReply::NetworkError >( "expectedError" );
	QTest::addColumn< QString >( "expectedErrorString" );
	QTest::addColumn< OptionalRegEx >( "expectedWarning" );

	const Request getReq( QNetworkRequest( QUrl( "http://example.com" ) ), QNetworkAccessManager::GetOperation );
	const Request getHttpsReq( QNetworkRequest( QUrl( "https://example.com" ) ), QNetworkAccessManager::GetOperation );

	const QString unknownError = "Unknown error";

	const OptionalRegEx noExpectedWarning = qMakePair( false, QRegularExpression() );

	#if QT_VERSION < QT_VERSION_CHECK( 5,6,0 )
		const QString httpErrorStringTemplate = "Error downloading %1 - server replied: %2";
	#else
		const QString httpErrorStringTemplate = "Error transferring %1 - server replied: %2";
	#endif // Qt >= 5.6.0

	const QNetworkReply::NetworkError noError = QNetworkReply::NoError;
	const QNetworkReply::NetworkError notFoundError = QNetworkReply::ContentNotFoundError;
	const QNetworkReply::NetworkError accessDeniedError = QNetworkReply::ContentAccessDenied;
	const QNetworkReply::NetworkError temporaryNetworkError = QNetworkReply::TemporaryNetworkFailureError;
#if QT_VERSION >= QT_VERSION_CHECK( 5,3,0 )
	const QNetworkReply::NetworkError internalServerError = QNetworkReply::InternalServerError;
	const HttpStatus::Code internalServErrorCode = HttpStatus::InternalServerError;
#endif // Qt >= 5.3.0
	const HttpStatus::Code successStatusCode = HttpStatus::OK;
	const HttpStatus::Code forbiddenStatusCode = HttpStatus::Forbidden;
	const HttpStatus::Code notFoundCode = HttpStatus::NotFound;
	const QByteArray customReasonPhrase( "foo" );
	const QByteArray body( "{\"status\":\"ok\"}" );
	const QByteArray contentLengthHeader( "Content-Length" );


	const BehaviorFlags expectedBehavior = Behavior_Expected;

	MockReplyBuilder simpleReplyBuilder;
	simpleReplyBuilder.withBody( body );
	AttributeHash defaultAttributes;
	defaultAttributes.insert( QNetworkRequest::HttpStatusCodeAttribute, successStatusCode );
	defaultAttributes.insert( QNetworkRequest::HttpReasonPhraseAttribute, HttpStatus::reasonPhrase( successStatusCode ).toLatin1() );
	defaultAttributes.insert( QNetworkRequest::ConnectionEncryptedAttribute, false );
	RawHeaderHash defaultRawHeaders;
	defaultRawHeaders.insert( contentLengthHeader, QByteArray::number( body.size() ) );
	HeaderHash defaultHeaders;
	defaultHeaders.insert( QNetworkRequest::ContentLengthHeader, body.size() );
	QTest::newRow( "set default properties" ) << simpleReplyBuilder << expectedBehavior  << getReq
		<< defaultAttributes << defaultRawHeaders << defaultHeaders << noError << unknownError << noExpectedWarning;

	AttributeHash additionalAttributes( defaultAttributes );
	additionalAttributes.insert( QNetworkRequest::ConnectionEncryptedAttribute, true );
	QTest::newRow( "additional attributes" ) << simpleReplyBuilder << expectedBehavior << getHttpsReq
		<< additionalAttributes << defaultRawHeaders << defaultHeaders << noError << unknownError << noExpectedWarning;

	MockReplyBuilder dontOverrideBuilder;
	const unsigned int fakeContentLength = 1337;
	const QString unusualErrorString( "unusual error" );
	dontOverrideBuilder.withError( notFoundError, unusualErrorString );
	dontOverrideBuilder.withError( notFoundError );
	dontOverrideBuilder.withBody( body ).withHeader( QNetworkRequest::ContentLengthHeader, fakeContentLength );
	dontOverrideBuilder.withAttribute( QNetworkRequest::HttpReasonPhraseAttribute, customReasonPhrase );
	dontOverrideBuilder.withStatus( notFoundCode );
	AttributeHash dontOverrideAttributes;
	dontOverrideAttributes.insert( QNetworkRequest::HttpStatusCodeAttribute, notFoundCode );
	dontOverrideAttributes.insert( QNetworkRequest::HttpReasonPhraseAttribute, customReasonPhrase );
	dontOverrideAttributes.insert( QNetworkRequest::ConnectionEncryptedAttribute, false );
	RawHeaderHash dontOverrideRawHeaders;
	dontOverrideRawHeaders.insert( contentLengthHeader, "1337" );
	HeaderHash dontOverrideHeaders;
	dontOverrideHeaders.insert( QNetworkRequest::ContentLengthHeader, fakeContentLength );
	QTest::newRow( "don't override explicitly set properties" ) << dontOverrideBuilder << expectedBehavior << getReq
		<< dontOverrideAttributes << dontOverrideRawHeaders << dontOverrideHeaders << notFoundError << unusualErrorString
		<< noExpectedWarning;

	MockReplyBuilder defaultErrorStringBuilder;
	defaultErrorStringBuilder.withError( notFoundError ).withStatus( HttpStatus::NotFound );
	AttributeHash notFoundStatusAttributes;
	notFoundStatusAttributes.insert( QNetworkRequest::HttpStatusCodeAttribute, notFoundCode );
	notFoundStatusAttributes.insert( QNetworkRequest::HttpReasonPhraseAttribute, HttpStatus::reasonPhrase( notFoundCode ).toLatin1() );
	notFoundStatusAttributes.insert( QNetworkRequest::ConnectionEncryptedAttribute, false );
	QTest::newRow( "set default error string" ) << defaultErrorStringBuilder << expectedBehavior << getReq
		<< notFoundStatusAttributes << RawHeaderHash() << HeaderHash() << notFoundError
		<< httpErrorStringTemplate.arg( "http://example.com", "Not Found" ) << noExpectedWarning;


	MockReplyBuilder invalidStatusCodeBuilder;
	invalidStatusCodeBuilder.withAttribute(QNetworkRequest::HttpStatusCodeAttribute, QByteArray("Foo"));
	QTest::newRow( "invalid status code attribute" ) << invalidStatusCodeBuilder << expectedBehavior << getReq
		<< defaultAttributes << RawHeaderHash() << HeaderHash() << noError << unknownError
		<< makeOptionalRegEx( "Invalid type for HttpStatusCodeAttribute: QByteArray" );

	MockReplyBuilder accessDeniedBuilder;
	accessDeniedBuilder.withError( accessDeniedError );
	AttributeHash forbiddenStatusAttributes;
	forbiddenStatusAttributes.insert( QNetworkRequest::HttpStatusCodeAttribute, forbiddenStatusCode );
	forbiddenStatusAttributes.insert( QNetworkRequest::HttpReasonPhraseAttribute, HttpStatus::reasonPhrase( forbiddenStatusCode ).toLatin1() );
	forbiddenStatusAttributes.insert( QNetworkRequest::ConnectionEncryptedAttribute, false );
	QTest::newRow( "set status code from client error" ) << accessDeniedBuilder << expectedBehavior << getReq
		<< forbiddenStatusAttributes << RawHeaderHash() << HeaderHash() << accessDeniedError
		<< httpErrorStringTemplate.arg( "http://example.com", "Forbidden" ) << noExpectedWarning;

#if QT_VERSION >= QT_VERSION_CHECK( 5,3,0 )
	MockReplyBuilder internalServerErrorBuilder;
	internalServerErrorBuilder.withError( internalServerError );
	AttributeHash internalServerErrorStatusAttributes;
	internalServerErrorStatusAttributes.insert( QNetworkRequest::HttpStatusCodeAttribute, internalServErrorCode );
	internalServerErrorStatusAttributes.insert( QNetworkRequest::HttpReasonPhraseAttribute, HttpStatus::reasonPhrase( internalServErrorCode ).toLatin1() );
	internalServerErrorStatusAttributes.insert( QNetworkRequest::ConnectionEncryptedAttribute, false );
	QTest::newRow( "set status code from server error" ) << internalServerErrorBuilder << expectedBehavior << getReq
		<< internalServerErrorStatusAttributes << RawHeaderHash() << HeaderHash() << internalServerError
		<< httpErrorStringTemplate.arg( "http://example.com", "Internal Server Error" ) << noExpectedWarning;
#endif // Qt >= 5.3.0

	MockReplyBuilder temporaryNetworkErrorBuilder;
	temporaryNetworkErrorBuilder.withError( temporaryNetworkError );
	AttributeHash temporaryNetworkErrorAttributes;
	temporaryNetworkErrorAttributes.insert( QNetworkRequest::HttpStatusCodeAttribute, QVariant() );
	temporaryNetworkErrorAttributes.insert( QNetworkRequest::HttpReasonPhraseAttribute, QVariant() );
	temporaryNetworkErrorAttributes.insert( QNetworkRequest::ConnectionEncryptedAttribute, false );
	QTest::newRow( "set status code from network error" ) << temporaryNetworkErrorBuilder << expectedBehavior << getReq
		<< temporaryNetworkErrorAttributes << RawHeaderHash() << HeaderHash() << temporaryNetworkError
		<< "Temporary network failure." << noExpectedWarning;

	MockReplyBuilder notFoundBuilder;
	notFoundBuilder.withStatus( notFoundCode );
	QTest::newRow( "set error from status code" ) << notFoundBuilder << expectedBehavior << getReq
		<< notFoundStatusAttributes << RawHeaderHash() << HeaderHash() << notFoundError
		<< httpErrorStringTemplate.arg( "http://example.com", "Not Found" ) << noExpectedWarning;

	AttributeHash fileDefaultAttributes;
	fileDefaultAttributes.insert( QNetworkRequest::ConnectionEncryptedAttribute, false );

	MockReplyBuilder fileGetSuccessBuilder;
	fileGetSuccessBuilder.withBody( body );
	QTest::newRow( "file GET success" ) << fileGetSuccessBuilder << expectedBehavior << fileGetRequest
		<< fileDefaultAttributes << RawHeaderHash() << defaultHeaders << noError << unknownError << noExpectedWarning;

	MockReplyBuilder fileHeadSuccessBuilder;
	fileHeadSuccessBuilder.withHeader( QNetworkRequest::ContentLengthHeader, body.size() );
	QTest::newRow( "file HEAD success" ) << fileHeadSuccessBuilder << expectedBehavior << fileHeadRequest
		<< fileDefaultAttributes << RawHeaderHash() << defaultHeaders << noError << unknownError << noExpectedWarning;

	MockReplyBuilder filePutSuccessBuilder;
	filePutSuccessBuilder.withError( noError );
	QTest::newRow( "file PUT success" ) << filePutSuccessBuilder << expectedBehavior << filePutRequest
		<< fileDefaultAttributes << RawHeaderHash() << HeaderHash() << noError << unknownError << noExpectedWarning;

	MockReplyBuilder filePutProtocolFailureBuilder;
	filePutProtocolFailureBuilder.withError( QNetworkReply::ProtocolFailure );
	QTest::newRow( "file PUT protocol failure" ) << filePutProtocolFailureBuilder << expectedBehavior << filePutRequest
		<< fileDefaultAttributes << RawHeaderHash() << HeaderHash() << QNetworkReply::ProtocolFailure
		<< QStringLiteral( "Write error writing to %1: %2" ).arg( fileUrl.toString(), unknownError )
		<< noExpectedWarning;


	const QString fileErrorStringTemplate = QStringLiteral( "Error opening %1: %2" ).arg( fileUrl.toString() );
	QTest::newRow( "file access denied" ) << accessDeniedBuilder << expectedBehavior << fileGetRequest
		<< fileDefaultAttributes << RawHeaderHash() << HeaderHash() << accessDeniedError
		<< fileErrorStringTemplate.arg( "Access denied" ) << noExpectedWarning;

	const Request fileDeleteRequest( QNetworkRequest( fileUrl ), QNetworkAccessManager::DeleteOperation );
	const Request filePostRequest( QNetworkRequest( fileUrl ), QNetworkAccessManager::PostOperation, body );
	const Request fileCustomRequest( QNetworkRequest( fileUrl ), QNetworkAccessManager::CustomOperation );
	MockReplyBuilder fileProtocolErrorBuilder;
	const QNetworkReply::NetworkError protocolUnknownError = QNetworkReply::ProtocolUnknownError;
	const QString fileProtocolUnknownError( "Protocol \"file\" is unknown" );
	fileProtocolErrorBuilder.withError( protocolUnknownError, fileProtocolUnknownError );
	QTest::newRow( "file DELETE" ) << fileProtocolErrorBuilder << expectedBehavior << fileDeleteRequest
		<< fileDefaultAttributes << RawHeaderHash() << HeaderHash() << protocolUnknownError << fileProtocolUnknownError
		<< noExpectedWarning;
	QTest::newRow( "file POST" ) << fileProtocolErrorBuilder << expectedBehavior << filePostRequest
		<< fileDefaultAttributes << RawHeaderHash() << HeaderHash() << protocolUnknownError << fileProtocolUnknownError
		<< noExpectedWarning;
	QTest::newRow( "file custom verb" ) << fileProtocolErrorBuilder << expectedBehavior << fileCustomRequest
		<< fileDefaultAttributes << RawHeaderHash() << HeaderHash() << protocolUnknownError << fileProtocolUnknownError
		<< noExpectedWarning;

	MockReplyBuilder fileDeleteSuccessBuilder;
	fileDeleteSuccessBuilder.withError( noError );
	QTest::newRow( "file DELETE success" ) << fileDeleteSuccessBuilder << expectedBehavior << fileDeleteRequest
		<< fileDefaultAttributes << RawHeaderHash() << HeaderHash() << protocolUnknownError << fileProtocolUnknownError
		<< makeOptionalRegEx( "Reply was configured to reply with error (?:0|QNetworkReply::(?:NetworkError\\()?NoError\\)?) "
		                      "but a request DELETE file:///path/to/file.txt must be replied with "
		                      "(?:301|QNetworkReply::(?:NetworkError\\()?ProtocolUnknownError\\)?) . "
		                      "Overriding configured behavior." );

	MockReplyBuilder qrcGetSuccessBuilder;
	qrcGetSuccessBuilder.withBody( body );
	QTest::newRow( "qrc GET success" ) << qrcGetSuccessBuilder << expectedBehavior << qrcGetRequest
		<< fileDefaultAttributes << RawHeaderHash() << defaultHeaders << noError << unknownError << noExpectedWarning;

	MockReplyBuilder qrcHeadSuccessBuilder;
	qrcHeadSuccessBuilder.withHeader( QNetworkRequest::ContentLengthHeader, body.size() );
	QTest::newRow( "qrc HEAD success" ) << qrcHeadSuccessBuilder << expectedBehavior << qrcHeadRequest
		<< fileDefaultAttributes << RawHeaderHash() << defaultHeaders << noError << unknownError << noExpectedWarning;

	MockReplyBuilder qrcPutSuccessBuilder;
	qrcPutSuccessBuilder.withError( noError );
	QTest::newRow( "qrc PUT success" ) << qrcPutSuccessBuilder << expectedBehavior << qrcPutRequest
		<< fileDefaultAttributes << RawHeaderHash() << HeaderHash() << accessDeniedError << unknownError
		<< makeOptionalRegEx( "Reply was configured to reply with error (?:0|QNetworkReply::(?:NetworkError\\()?NoError\\)?) "
		                      "but a qrc request does not support writing operations and therefore has to reply with "
		                      "(?:201|QNetworkReply::(?:NetworkError\\()?ContentAccessDenied\\)?) . "
		                      "Overriding configured behavior." );

	MockReplyBuilder qrcPutFailureBuilder;
	qrcPutFailureBuilder.withError( accessDeniedError, "Custom error message" );
	QTest::newRow( "qrc PUT failure" ) << qrcPutFailureBuilder << expectedBehavior << qrcPutRequest
		<< fileDefaultAttributes << RawHeaderHash() << HeaderHash() << accessDeniedError << "Custom error message"
		<< noExpectedWarning;

	const QString qrcErrorStringTemplate = QStringLiteral( "Error opening %1: %2" ).arg( qrcUrl.toString() );
	QTest::newRow( "qrc access denied" ) << accessDeniedBuilder << expectedBehavior << qrcGetRequest
		<< fileDefaultAttributes << RawHeaderHash() << HeaderHash() << accessDeniedError
		<< qrcErrorStringTemplate.arg( unknownError ) << noExpectedWarning;

	const Request qrcDeleteRequest( QNetworkRequest( qrcUrl ), QNetworkAccessManager::DeleteOperation );
	const Request qrcPostRequest( QNetworkRequest( qrcUrl ), QNetworkAccessManager::PostOperation, body );
	const Request qrcCustomRequest( QNetworkRequest( qrcUrl ), QNetworkAccessManager::CustomOperation );
	MockReplyBuilder qrcProtocolErrorBuilder;
	const QString qrcProtocolUnknownError( "Protocol \"qrc\" is unknown" );
	qrcProtocolErrorBuilder.withError( protocolUnknownError, qrcProtocolUnknownError );
	QTest::newRow( "qrc DELETE" ) << qrcProtocolErrorBuilder << expectedBehavior << qrcDeleteRequest
		<< fileDefaultAttributes << RawHeaderHash() << HeaderHash() << protocolUnknownError << qrcProtocolUnknownError
		<< noExpectedWarning;
	QTest::newRow( "qrc POST" ) << qrcProtocolErrorBuilder << expectedBehavior << qrcPostRequest
		<< fileDefaultAttributes << RawHeaderHash() << HeaderHash() << protocolUnknownError << qrcProtocolUnknownError
		<< noExpectedWarning;
	QTest::newRow( "qrc custom verb" ) << qrcProtocolErrorBuilder << expectedBehavior << qrcCustomRequest
		<< fileDefaultAttributes << RawHeaderHash() << HeaderHash() << protocolUnknownError << qrcProtocolUnknownError
		<< noExpectedWarning;

	MockReplyBuilder qrcPostSuccessBuilder;
	qrcPostSuccessBuilder.withError( noError );
	QTest::newRow( "qrc POST success" ) << qrcPostSuccessBuilder << expectedBehavior << qrcPostRequest
		<< fileDefaultAttributes << RawHeaderHash() << HeaderHash() << protocolUnknownError << qrcProtocolUnknownError
		<< makeOptionalRegEx( "Reply was configured to reply with error (?:0|QNetworkReply::(?:NetworkError\\()?NoError\\)?) "
		                      "but a request POST qrc:/path/to/file.txt must be replied with "
		                      "(?:301|QNetworkReply::(NetworkError\\()?ProtocolUnknownError\\)?) . "
		                      "Overriding configured behavior." );
}


/*! \test Tests the default error strings produced by MockReply::prepare().
 */
void MockReplyTest::testDefaultErrorString()
{
	QFETCH( MockReplyBuilder, builder );
	QFETCH( Request, request );

	MockReplyUniquePtr reply( builder.createReply() );

	MockReplyTestHelper::callPrepare( reply.get(), request );

	QTEST( reply->errorString(), "expectedErrorString" );
}


/*! Provides the data for the testDefaultErrorString() test.
 */
void MockReplyTest::testDefaultErrorString_data()
{
	QTest::addColumn< MockReplyBuilder >( "builder" );
	QTest::addColumn< Request >( "request" );
	QTest::addColumn< QString >( "expectedErrorString" );

	const Request httpRequest( QNetworkRequest( QUrl( "http://example.com" ) ), QNetworkAccessManager::GetOperation );
	const Request ftpRequest( QNetworkRequest( QUrl( "ftp://example.com" ) ), QNetworkAccessManager::GetOperation );
	const QString filePath = fileGetRequest.qRequest.url().toString();

	using namespace HelperMethods;

	QTest::newRow( "http remote host closed" ) << createErrorBuilder( QNetworkReply::RemoteHostClosedError ) << httpRequest << "Connection closed";
	QTest::newRow( "http operation canceled" ) << createErrorBuilder( QNetworkReply::OperationCanceledError ) << httpRequest << "Operation canceled";
	QTest::newRow( "http connection refused" ) << createErrorBuilder( QNetworkReply::ConnectionRefusedError ) << httpRequest << "Connection refused";
	QTest::newRow( "http ssl handshake failed" ) << createErrorBuilder( QNetworkReply::SslHandshakeFailedError ) << httpRequest << "SSL handshake failed";
	QTest::newRow( "http network session failed" ) << createErrorBuilder( QNetworkReply::NetworkSessionFailedError ) << httpRequest << "Network session error.";
	QTest::newRow( "http background request" ) << createErrorBuilder( QNetworkReply::BackgroundRequestNotAllowedError ) << httpRequest << "Background request not allowed.";
	#if QT_VERSION >= QT_VERSION_CHECK( 5,6,0 )
		QTest::newRow( "http too many redirects" ) << createErrorBuilder( QNetworkReply::TooManyRedirectsError ) << httpRequest << "Too many redirects";
		QTest::newRow( "http insecure redirect" ) << createErrorBuilder( QNetworkReply::InsecureRedirectError ) << httpRequest << "Insecure redirect";
	#endif
	QTest::newRow( "http proxy connection refused" ) << createErrorBuilder( QNetworkReply::ProxyConnectionRefusedError ) << httpRequest << "Proxy connection refused";
	QTest::newRow( "http proxy connection closed" ) << createErrorBuilder( QNetworkReply::ProxyConnectionClosedError ) << httpRequest << "Proxy connection closed prematurely";
	QTest::newRow( "http proxy not found" ) << createErrorBuilder( QNetworkReply::ProxyNotFoundError ) << httpRequest << "No suitable proxy found";
	QTest::newRow( "http proxy timeout" ) << createErrorBuilder( QNetworkReply::ProxyTimeoutError ) << httpRequest << "Proxy server connection timed out";
	QTest::newRow( "http protocol unknown" ) << createErrorBuilder( QNetworkReply::ProtocolUnknownError ) << httpRequest << "Unknown protocol specified";
	QTest::newRow( "http unknown network error" ) << createErrorBuilder( QNetworkReply::UnknownNetworkError ) << httpRequest << "Unknown network error";
	QTest::newRow( "http unknown proxy error" ) << createErrorBuilder( QNetworkReply::UnknownProxyError ) << httpRequest << "Unknown proxy error";
	QTest::newRow( "http timeout" ) << createErrorBuilder( QNetworkReply::TimeoutError ) << httpRequest << "Socket operation timed out";
	QTest::newRow( "http authentication required" ) << createErrorBuilder( QNetworkReply::AuthenticationRequiredError ) << httpRequest << "Host requires authentication";
	QTest::newRow( "http authentication required" ) << createErrorBuilder( QNetworkReply::AuthenticationRequiredError ) << httpRequest << "Host requires authentication";
	QTest::newRow( "ftp remote host closed" )  << createErrorBuilder( QNetworkReply::RemoteHostClosedError ) << ftpRequest  << "Connection closed";
	QTest::newRow( "ftp connection refused" )  << createErrorBuilder( QNetworkReply::ConnectionRefusedError ) << ftpRequest  << "Connection refused to host example.com";
	QTest::newRow( "ftp timeout" )  << createErrorBuilder( QNetworkReply::TimeoutError ) << ftpRequest  << "Connection timed out to host example.com";
	QTest::newRow( "ftp authentication required" )  << createErrorBuilder( QNetworkReply::AuthenticationRequiredError ) << ftpRequest  << "Logging in to example.com failed: authentication required";
	QTest::newRow( "file protocol unknown" ) << createErrorBuilder( QNetworkReply::ProtocolUnknownError ) << fileGetRequest << "Protocol \"file\" is unknown";
	QTest::newRow( "file operation not permitted" ) << createErrorBuilder( QNetworkReply::ContentOperationNotPermittedError ) << fileGetRequest \
		<< QStringLiteral( "Cannot open %1: Path is a directory" ).arg( filePath );
	QTest::newRow( "file content not found" ) << createErrorBuilder( QNetworkReply::ContentNotFoundError ) << fileGetRequest \
		<< QStringLiteral( "Error opening %1: No such file or directory" ).arg( filePath );
	QTest::newRow( "file access denied" ) << createErrorBuilder( QNetworkReply::ContentAccessDenied ) << fileGetRequest \
		<< QStringLiteral( "Error opening %1: Access denied" ).arg( filePath );
	QTest::newRow( "file read error" ) << createErrorBuilder( QNetworkReply::ProtocolFailure ) << fileGetRequest \
		<< QStringLiteral( "Read error reading from %1: Unknown error" ).arg( filePath );

}


/*! \test Tests the MockReply::finish() method.
 */
void MockReplyTest::testFinish()
{
	QFETCH( MockReplyBuilder, builder );
	QFETCH( Request, request );
	QFETCH( QByteArray, replyPayload );
	QFETCH( BehaviorFlags, behaviorFlags );

	MockReplyUniquePtr reply( builder.createReply() );

	MockReplyTestHelper::callSetBehaviorFlags( reply.get(), behaviorFlags );

	QSignalInspector inspector( reply.get() );

	const QString errorString = reply->errorString();

	MockReplyTestHelper::callFinish( reply.get(), request );
	QTest::qWait( 100 );

	QVERIFY( reply->isFinished() );
	QCOMPARE( reply->bytesAvailable(), static_cast< qint64 >( replyPayload.size() ) );
	QVERIFY( reply->isReadable() );
	QVERIFY( reply->isOpen() );
	QCOMPARE( reply->readAll(), replyPayload );
	QCOMPARE( reply->errorString(), errorString );

	SignalEmissionList emissions = getSignalEmissionListFromInspector( inspector );

	QTEST( emissions, "expectedSignals" );
}

/*! Provides the data for the testFinish() test.
 */
void MockReplyTest::testFinish_data()
{
	QTest::addColumn< MockReplyBuilder >( "builder" );
	QTest::addColumn< Request >( "request" );
	QTest::addColumn< QByteArray >( "replyPayload" );
	QTest::addColumn< BehaviorFlags >( "behaviorFlags" );
	QTest::addColumn< SignalEmissionList >( "expectedSignals" );

	// Possible signals
	const QMetaObject& replyMetaObj = QNetworkReply::staticMetaObject;

	QMetaMethod metaDataChangedSignal =     getSignal( replyMetaObj, "metaDataChanged()" );
	QMetaMethod uploadProgressSignal =      getSignal( replyMetaObj, "uploadProgress(qint64, qint64)" );
	QMetaMethod downloadProgressSignal =    getSignal( replyMetaObj, "downloadProgress(qint64, qint64)" );
	QMetaMethod finishedSignal =            getSignal( replyMetaObj, "finished()" );
	#if QT_VERSION < QT_VERSION_CHECK( 6,0,0 )
		QMetaMethod errorSignal =           getSignal( replyMetaObj, "error(QNetworkReply::NetworkError)" );
	#endif // Qt < 6.0.0
	#if QT_VERSION >= QT_VERSION_CHECK( 5,15,0 )
		QMetaMethod errorOccurredSignal =   getSignal( replyMetaObj, "errorOccurred(QNetworkReply::NetworkError)" );
	#endif // Qt >= 5.15.0
	QMetaMethod readyReadSignal =           getSignal( replyMetaObj, "readyRead()" );
	QMetaMethod readChannelFinishedSignal = getSignal( replyMetaObj, "readChannelFinished()" );

	const BehaviorFlags expectedBehavior = Behavior_Expected;

	const Request getReq( QNetworkRequest( QUrl( "http://example.com" ) ) );
	#if QT_VERSION >= QT_VERSION_CHECK( 5,2,2 )
		const QByteArray successPayload( "{\"status\":\"ok\"}" );
	#else
		// QTBUG-36682
		const QByteArray successPayload( "{\"status\": \"ok\"}" );
	#endif // Qt < 5.2.2

	MockReplyBuilder successBuilder;
	successBuilder.withStatus( HttpStatus::OK ).withBody( QJsonDocument::fromJson( successPayload ) );
	QTest::newRow( "succeeding, finished GET" ) << successBuilder << getReq << successPayload << expectedBehavior <<
	( SignalEmissionList() << qMakePair( metaDataChangedSignal, QVariantList() )
	                       << qMakePair( readyReadSignal, QVariantList() )
	                       << qMakePair( downloadProgressSignal, QVariantList() << successPayload.size() << successPayload.size() )
	                       << qMakePair( downloadProgressSignal, QVariantList() << successPayload.size() << successPayload.size() )
	                       << qMakePair( readChannelFinishedSignal, QVariantList() )
	                       << qMakePair( finishedSignal, QVariantList() ) );

	#if QT_VERSION >= QT_VERSION_CHECK( 5,2,2 )
		const QByteArray requestPayload( "{\"request\":\"foo\"}" );
	#else
		// QTBUG-36682
		const QByteArray requestPayload( "{\"request\": \"foo\"}" );
	#endif // Qt < 5.2.2

	const Request postReq( QNetworkRequest( QUrl( "http://example.com" ) ), QNetworkAccessManager::PostOperation, requestPayload );
	QTest::newRow( "succeeding, finished POST" ) << successBuilder << postReq << successPayload << expectedBehavior <<
	( SignalEmissionList() << qMakePair( uploadProgressSignal, QVariantList() << postReq.body.size() << postReq.body.size() )
	                       << qMakePair( metaDataChangedSignal, QVariantList() )
	                       << qMakePair( readyReadSignal, QVariantList() )
	                       << qMakePair( downloadProgressSignal, QVariantList() << successPayload.size() << successPayload.size() )
	                       << qMakePair( downloadProgressSignal, QVariantList() << successPayload.size() << successPayload.size() )
	                       << qMakePair( readChannelFinishedSignal, QVariantList() )
	                       << qMakePair( finishedSignal, QVariantList() ) );

	MockReplyBuilder failureBuilder;
	failureBuilder.withStatus( HttpStatus::NotFound );
	failureBuilder.withError( HttpStatus::statusCodeToNetworkError( HttpStatus::NotFound ),
	                          HttpStatus::reasonPhrase( HttpStatus::NotFound ) ); // explicit error string (see issue #37)
	QTest::newRow( "failing, finished GET" ) << failureBuilder << getReq << QByteArray() << expectedBehavior <<
	( SignalEmissionList() << qMakePair( metaDataChangedSignal, QVariantList() )
		#if QT_VERSION < QT_VERSION_CHECK( 6,0,0 )
	                       << qMakePair( errorSignal, QVariantList() << QVariant::fromValue( QNetworkReply::ContentNotFoundError ) )
		#endif // Qt < 6.0.0
		#if QT_VERSION >= QT_VERSION_CHECK( 5,15,0 )
	                       << qMakePair( errorOccurredSignal, QVariantList() << QVariant::fromValue( QNetworkReply::ContentNotFoundError ) )
		#endif // Qt >= 5.15.0
	                       << qMakePair( downloadProgressSignal, QVariantList() << 0 << 0 )
	                       << qMakePair( readChannelFinishedSignal, QVariantList() )
	                       << qMakePair( finishedSignal, QVariantList() ) );

	QTest::newRow( "failing, finished POST" ) << failureBuilder << postReq << QByteArray() << expectedBehavior <<
	( SignalEmissionList() << qMakePair( uploadProgressSignal, QVariantList() << postReq.body.size() << postReq.body.size() )
	                       << qMakePair( metaDataChangedSignal, QVariantList() )
		#if QT_VERSION < QT_VERSION_CHECK( 6,0,0 )
	                       << qMakePair( errorSignal, QVariantList() << QVariant::fromValue( QNetworkReply::ContentNotFoundError ) )
		#endif // Qt < 6.0.0
		#if QT_VERSION >= QT_VERSION_CHECK( 5,15,0 )
	                       << qMakePair( errorOccurredSignal, QVariantList() << QVariant::fromValue( QNetworkReply::ContentNotFoundError ) )
		#endif // Qt >= 5.15.0
	                       << qMakePair( downloadProgressSignal, QVariantList() << 0 << 0 )
	                       << qMakePair( readChannelFinishedSignal, QVariantList() )
	                       << qMakePair( finishedSignal, QVariantList() ) );


	QTest::newRow( "file, succeeding GET" ) << successBuilder << fileGetRequest << successPayload << expectedBehavior << SignalEmissionList();
	QTest::newRow( "file, succeeding HEAD" ) << successBuilder << fileHeadRequest << QByteArray() << expectedBehavior << SignalEmissionList();

	MockReplyBuilder fileNotExistsBuilder;
	fileNotExistsBuilder.withError( QNetworkReply::ContentNotFoundError,
	                                QStringLiteral( "Error opening %1: No such file or directory" ).arg( fileUrl.path() ) );
	QTest::newRow( "file, failing GET" ) << fileNotExistsBuilder << fileGetRequest << QByteArray() << expectedBehavior << SignalEmissionList();
	QTest::newRow( "file, failing HEAD" ) << fileNotExistsBuilder << fileHeadRequest << QByteArray() << expectedBehavior << SignalEmissionList();


	QTest::newRow( "file, succeeding PUT" ) << successBuilder << filePutRequest << QByteArray() << expectedBehavior <<
	( SignalEmissionList() << qMakePair( uploadProgressSignal, QVariantList() << filePutRequest.body.size() << filePutRequest.body.size() )
	                       << qMakePair( downloadProgressSignal, QVariantList() << 0 << 0 )
	                       << qMakePair( readChannelFinishedSignal, QVariantList() )
	                       << qMakePair( finishedSignal, QVariantList() ) );

	const Request fileEmptyPutRequest( QNetworkRequest( fileUrl ), QNetworkAccessManager::PutOperation );
	QTest::newRow( "file, succeeding empty PUT" ) << successBuilder << fileEmptyPutRequest << QByteArray() << expectedBehavior <<
	( SignalEmissionList() << qMakePair( downloadProgressSignal, QVariantList() << 0 << 0 )
		                   << qMakePair( uploadProgressSignal, QVariantList() << 0 << 0 )
		                   << qMakePair( readChannelFinishedSignal, QVariantList() )
		                   << qMakePair( finishedSignal, QVariantList() ) );


	MockReplyBuilder fileAccessDeniedBuilder;
	fileAccessDeniedBuilder.withError( QNetworkReply::ContentAccessDenied, QStringLiteral( "Error opening %1: Access denied" ).arg( fileUrl.toString() ) );
	SignalEmissionList fileAccessDeniedSignals;
	fileAccessDeniedSignals
		#if QT_VERSION < QT_VERSION_CHECK( 6,0,0 )
		 << qMakePair( errorSignal, QVariantList() << QVariant::fromValue( QNetworkReply::ContentAccessDenied ) )
		#endif // Qt < 6.0.0
		#if QT_VERSION >= QT_VERSION_CHECK( 5,15,0 )
		 << qMakePair( errorOccurredSignal, QVariantList() << QVariant::fromValue( QNetworkReply::ContentAccessDenied ) )
		#endif // Qt >= 5.15.0
		 << qMakePair( downloadProgressSignal, QVariantList() << 0 << 0 )
		 << qMakePair( uploadProgressSignal, QVariantList() << 0 << 0 )
		 << qMakePair( readChannelFinishedSignal, QVariantList() )
		 << qMakePair( finishedSignal, QVariantList() );
	QTest::newRow( "file, failing PUT" ) << fileAccessDeniedBuilder << filePutRequest << QByteArray() << expectedBehavior <<
		fileAccessDeniedSignals;

	MockReplyBuilder fileProtocolErrorBuilder;
	fileProtocolErrorBuilder.withError( QNetworkReply::ProtocolUnknownError, "Protocol \"file\" is unknown" );
	SignalEmissionList protocolUnknownSignals;
	protocolUnknownSignals
		#if QT_VERSION < QT_VERSION_CHECK( 6,0,0 )
		 << qMakePair( errorSignal, QVariantList() << QVariant::fromValue( QNetworkReply::ProtocolUnknownError ) )
		#endif // Qt < 6.0.0
		#if QT_VERSION >= QT_VERSION_CHECK( 5,15,0 )
		 << qMakePair( errorOccurredSignal, QVariantList() << QVariant::fromValue( QNetworkReply::ProtocolUnknownError ) )
		#endif // Qt >= 5.15.0
		 << qMakePair( downloadProgressSignal, QVariantList() << 0 << 0 )
		 << qMakePair( readChannelFinishedSignal, QVariantList() )
		 << qMakePair( finishedSignal, QVariantList() );
	const Request filePostRequest( QNetworkRequest( fileUrl ), QNetworkAccessManager::PostOperation );
	const Request fileDeleteRequest( QNetworkRequest( fileUrl ), QNetworkAccessManager::DeleteOperation );
	QTest::newRow( "file POST" ) << fileProtocolErrorBuilder << filePostRequest << QByteArray() << expectedBehavior <<
		protocolUnknownSignals;
	QTest::newRow( "file DELETE" ) << fileProtocolErrorBuilder << fileDeleteRequest << QByteArray() << expectedBehavior <<
		protocolUnknownSignals;

	QTest::newRow( "file POST, final upload00" ) << fileProtocolErrorBuilder << filePostRequest << QByteArray() <<
		( expectedBehavior | Behavior_FinalUpload00Signal ) <<
	( SignalEmissionList()
		#if QT_VERSION < QT_VERSION_CHECK( 6,0,0 )
		 << qMakePair( errorSignal, QVariantList() << QVariant::fromValue( QNetworkReply::ProtocolUnknownError ) )
		#endif // Qt < 6.0.0
		#if QT_VERSION >= QT_VERSION_CHECK( 5,15,0 )
		 << qMakePair( errorOccurredSignal, QVariantList() << QVariant::fromValue( QNetworkReply::ProtocolUnknownError ) )
		#endif // Qt >= 5.15.0
		 << qMakePair( downloadProgressSignal, QVariantList() << 0 << 0 )
		 << qMakePair( uploadProgressSignal, QVariantList() << 0 << 0 )
		 << qMakePair( readChannelFinishedSignal, QVariantList() )
		 << qMakePair( finishedSignal, QVariantList() ) );

	const Request qrcGetRequest( QNetworkRequest( qrcUrl ), QNetworkAccessManager::GetOperation );
	QTest::newRow( "qrc, succeeding GET" ) << successBuilder << qrcGetRequest << successPayload << expectedBehavior <<
		SignalEmissionList();


	MockReplyBuilder qrcAccessDeniedBuilder;
	qrcAccessDeniedBuilder.withError( QNetworkReply::ContentAccessDenied, QStringLiteral( "Error opening %1: Unknown error" ).arg( qrcUrl.toString() ) );
	const Request qrcPutRequest( QNetworkRequest( qrcUrl ), QNetworkAccessManager::PutOperation );
	QTest::newRow( "qrc PUT" ) << qrcAccessDeniedBuilder << qrcPutRequest << QByteArray() << expectedBehavior <<
		fileAccessDeniedSignals;


	MockReplyBuilder qrcProtocolErrorBuilder;
	qrcProtocolErrorBuilder.withError( QNetworkReply::ProtocolUnknownError, "Protocol \"qrc\" is unknown" );
	const Request qrcPostRequest( QNetworkRequest( qrcUrl ), QNetworkAccessManager::PostOperation );
	const Request qrcDeleteRequest( QNetworkRequest( qrcUrl ), QNetworkAccessManager::DeleteOperation );
	QTest::newRow( "qrc POST" ) << qrcProtocolErrorBuilder << qrcPostRequest << QByteArray() << expectedBehavior <<
		protocolUnknownSignals;
	QTest::newRow( "qrc DELETE" ) << qrcProtocolErrorBuilder << qrcDeleteRequest << QByteArray() << expectedBehavior <<
		protocolUnknownSignals;
}

/*! \test Tests the MockReply::abort() method.
 */
void MockReplyTest::testAbort()
{
	MockReplyBuilder builder;
	builder.withStatus(HttpStatus::OK);

	MockReplyUniquePtr reply(builder.createReply());

	QVERIFY(reply->isRunning());
	QVERIFY(!reply->isFinished());
	QCOMPARE(reply->error(), QNetworkReply::NoError);

	reply->abort();

	QVERIFY(!reply->isRunning());
	QVERIFY(reply->isFinished());
	QVERIFY(!reply->isOpen());
	QCOMPARE(reply->error(), QNetworkReply::OperationCanceledError);
}

/*! \test Tests the behavior of an unconfigured MockReplyBuilder.
 */
void MockReplyTest::testNullBuilder()
{
	MockReplyBuilder builder;
	QCOMPARE(builder.createReply(), static_cast<MockReply*>(Q_NULLPTR));
}

/*! Provides the data for the testWithBodyByteArray() test.
 */
void MockReplyTest::testWithBodyByteArray_data()
{
	QTest::addColumn<QByteArray>("body");
	QTest::addColumn<QVariant>("finishedContentLength");
	QTest::addColumn<QVariant>("finishedContentType");

	QTest::newRow("null body")  << QByteArray()   << QVariant()  << QVariant();
	QTest::newRow("empty body") << QByteArray("") << QVariant(0) << QVariant();

	QByteArray plainText("foo bar");
	QTest::newRow("plain text") << plainText      << QVariant::fromValue(plainText.size()) << QVariant::fromValue(contentTypeTextPlain);
}

/*! \test Tests the MockReplyBuilder::withBody(const QByteArray&) method.
 */
void MockReplyTest::testWithBodyByteArray()
{
	QFETCH(QByteArray, body);

	MockReplyBuilder builder;
	builder.withBody(body);

	MockReplyUniquePtr reply(builder.createReply());

	MockReplyTestHelper::callPrepare(reply.get(), Request(QNetworkRequest()));
	MockReplyTestHelper::callFinish(reply.get(), Request(QNetworkRequest()));

	QCOMPARE(reply->bytesAvailable(), body.size());
	QCOMPARE(reply->body(), body);
	QCOMPARE(reply->readAll(), body);

	MockReplyTestHelper::callFinish(reply.get(), Request(QNetworkRequest()));
	QTEST(reply->header(QNetworkRequest::ContentLengthHeader), "finishedContentLength");
	QTEST(reply->header(QNetworkRequest::ContentTypeHeader), "finishedContentType");
}

/*! Provides the data for the testWithBodyJson() test.
 */
void MockReplyTest::testWithBodyJson_data()
{
	QTest::addColumn< QJsonDocument >( "body" );
	QTest::addColumn< QVariant >( "finishedContentLength" );

	QTest::newRow( "null body" )  << QJsonDocument()                     << QVariant();
	QTest::newRow( "empty body" ) << QJsonDocument::fromJson( "" )       << QVariant();
#if QT_VERSION >= QT_VERSION_CHECK( 5,2,2 )
	const QByteArray fooBarJson( "{\"foo\":\"bar\"}" );
#else
	// QTBUG-36682
	const QByteArray fooBarJson( "{\"foo\": \"bar\"}" );
#endif
	QTest::newRow( "object" )     << QJsonDocument::fromJson(fooBarJson) << QVariant(fooBarJson.size());
}

/*! \test Tests the MockReplyBuilder::withBody(const QJsonDocument&) method.
 */
void MockReplyTest::testWithBodyJson()
{
	QFETCH( QJsonDocument, body );

	MockReplyBuilder builder;
	builder.withBody( body );

	MockReplyUniquePtr reply( builder.createReply() );
	MockReplyTestHelper::callPrepare( reply.get(), Request( QNetworkRequest() ) );
	MockReplyTestHelper::callFinish( reply.get(), Request( QNetworkRequest() ) );

	QByteArray rawBody = body.toJson( QJsonDocument::Compact );
	QCOMPARE( reply->bytesAvailable(), rawBody.size() );
	QCOMPARE( reply->readAll(), rawBody);
	QCOMPARE( reply->header( QNetworkRequest::ContentTypeHeader ).toString(), contentTypeJson );
	QTEST( reply->header( QNetworkRequest::ContentLengthHeader ), "finishedContentLength" );
}

/*! \test Tests the MockReplyBuilder::withFile() method.
 */
void MockReplyTest::testWithFile()
{
	QFETCH( QString, file );

	QString filePath;
	if ( QTest::currentDataTag() == QString( "non existing file" ) )
		filePath = file;
	else
	{
		filePath = QFINDTESTDATA( file );
		Q_ASSERT( !filePath.isEmpty() );
	}

	MockReplyBuilder builder;

	if ( QTest::currentDataTag() == QString( "non existing file" ) )
	{
		#if QT_VERSION >= QT_VERSION_CHECK( 5,5,0 )
			const QString warningMsg = QString( "QIODevice::read (QFile, \"%1\"): device not open" ).arg( QDir::toNativeSeparators( filePath ) );
		#else
			const QString warningMsg = QString( "QIODevice::read: device not open" );
		#endif
		// Don't use Tests::ignoreMessage() here because this message is not affected by QTBUG-15256
		QTest::ignoreMessage( QtWarningMsg, warningMsg.toLatin1() );
	}

	builder.withFile( filePath );

	MockReplyUniquePtr reply( builder.createReply() );

	QFile fileObj( filePath );
	fileObj.open( QIODevice::ReadOnly );

	QByteArray fileContent = fileObj.readAll();

	MockReplyTestHelper::callPrepare( reply.get(), Request( QNetworkRequest() ) );
	MockReplyTestHelper::callFinish( reply.get(), Request( QNetworkRequest() ) );

	QCOMPARE( reply->bytesAvailable(), fileContent.size() );
	QCOMPARE( reply->readAll(), fileContent );
	QTEST( reply->header( QNetworkRequest::ContentTypeHeader ).toString(), "contentType" );
}

/*! Provides the data for the testWithFile() test.
 */
void MockReplyTest::testWithFile_data()
{
	QTest::addColumn< QString >( "file" );
	QTest::addColumn< QString >( "contentType" );

	QTest::newRow( "plain text" )        << "data/plainText.txt"                << contentTypeTextPlain;
	QTest::newRow( "xml" )               << "data/simple.xml"                   << "application/xml";
	QTest::newRow( "empty text file" )   << "data/empty.txt"                    << contentTypeTextPlain;
	QTest::newRow( "non existing file" ) << "data/this_file_does_not_exist.foo" << QString();
}

/*! Provides the data for the testWithStatus() test.
 */
void MockReplyTest::testWithStatus_data()
{
	QTest::addColumn<int>("statusCode");
	QTest::addColumn<QString>("reasonPhrase");
	QTest::addColumn<QNetworkReply::NetworkError>("error");

	QTest::newRow("success")                         << static_cast<int>(HttpStatus::OK)                << "OK"         << QNetworkReply::NoError;
	QTest::newRow("Qt unknown content error")        << static_cast<int>(HttpStatus::ExpectationFailed) << QString()    << QNetworkReply::UnknownContentError;
#if QT_VERSION >= QT_VERSION_CHECK(5,3,0)
	QTest::newRow("Qt unknown server error")         << static_cast<int>(HttpStatus::NotExtended)       << QString()    << QNetworkReply::UnknownServerError;
#endif // Qt >= 5.3.0
	QTest::newRow("custom error code")               << 666                                             << QString()    << QNetworkReply::ProtocolFailure;
	QTest::newRow("default reason phrase")           << static_cast<int>(HttpStatus::Created)           << QString()    << QNetworkReply::NoError;
	QTest::newRow("custom reason phrase")            << static_cast<int>(HttpStatus::MovedPermanently)  << "Go there"   << QNetworkReply::NoError;
	QTest::newRow("error")                           << static_cast<int>(HttpStatus::NotFound)          << QString()    << QNetworkReply::ContentNotFoundError;
	QTest::newRow("error with custom reason phrase") << static_cast<int>(HttpStatus::BadRequest)        << "Oops Error" << QNetworkReply::ProtocolInvalidOperationError;
}

/*! \test Tests the MockReplyBuilder::withStatus() method.
 */
void MockReplyTest::testWithStatus()
{
	QFETCH(int, statusCode);
	QFETCH(QString, reasonPhrase);

	MockReplyBuilder builder;
	builder.withStatus(statusCode, reasonPhrase);

	MockReplyUniquePtr reply(builder.createReply());

	MockReplyTestHelper::callPrepare(reply.get(), Request(QNetworkRequest()));
	QCOMPARE(reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt(), statusCode);
	if (reasonPhrase.isNull())
		QCOMPARE(reply->attribute(QNetworkRequest::HttpReasonPhraseAttribute).toString(), HttpStatus::reasonPhrase(statusCode));
	else
		QCOMPARE(reply->attribute(QNetworkRequest::HttpReasonPhraseAttribute).toString(), reasonPhrase);
	QTEST(reply->error(), "error");

	// Finishing the reply should not change anything
	MockReplyTestHelper::callFinish(reply.get(), Request(QNetworkRequest()));
	QCOMPARE(reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt(), statusCode);
	if (reasonPhrase.isNull())
		QCOMPARE(reply->attribute(QNetworkRequest::HttpReasonPhraseAttribute).toString(), HttpStatus::reasonPhrase(statusCode));
	else
		QCOMPARE(reply->attribute(QNetworkRequest::HttpReasonPhraseAttribute).toString(), reasonPhrase);
	QTEST(reply->error(), "error");
}

/*! Provides the data for the testWithError() test.
 */
void MockReplyTest::testWithError_data()
{
	QTest::addColumn<QNetworkReply::NetworkError>("error");
	QTest::addColumn<QString>("errorMsg");
	QTest::addColumn<int>("finishedStatusCode");

	QTest::newRow("no error")              << QNetworkReply::NoError              << QString("There is no error") << static_cast<int>(HttpStatus::OK);
	QTest::newRow("content not found")     << QNetworkReply::ContentNotFoundError << QString("Content not found") << static_cast<int>(HttpStatus::NotFound);
	QTest::newRow("unknown content error") << QNetworkReply::UnknownContentError  << QString("Broken content")    << static_cast<int>(HttpStatus::BadRequest);
#if QT_VERSION >= QT_VERSION_CHECK(5,3,0)
	QTest::newRow("unknown server error")  << QNetworkReply::UnknownServerError   << QString("Unknown Error")     << static_cast<int>(HttpStatus::InternalServerError);
#endif // Qt >= 5.3.0
	QTest::newRow("host not found")        << QNetworkReply::HostNotFoundError    << QString("Host not found")    << -1;
}

/*! \test Tests the MockReplyBuilder::withError() method.
 */
void MockReplyTest::testWithError()
{
	QFETCH(QNetworkReply::NetworkError, error);
	QFETCH(QString, errorMsg);
	QFETCH(int, finishedStatusCode);

	MockReplyBuilder builder;
	builder.withError(error, errorMsg);

	MockReplyUniquePtr reply(builder.createReply());

	QCOMPARE(reply->error(), error);
	QCOMPARE(reply->errorString(), errorMsg);
	QVERIFY(!reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).isValid());

	MockReplyTestHelper::callPrepare(reply.get(), Request(QNetworkRequest()));
	MockReplyTestHelper::callFinish(reply.get(), Request(QNetworkRequest()));
	if (finishedStatusCode < 0)
		QVERIFY(!reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).isValid());
	else
	{
		QVERIFY(reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).isValid());
		QCOMPARE(reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt(), finishedStatusCode);
		QCOMPARE(reply->attribute(QNetworkRequest::HttpReasonPhraseAttribute).toString(), HttpStatus::reasonPhrase(finishedStatusCode));
	}
}

/*! \test Tests the MockReplyBuilder::withRedirect() method.
 */
void MockReplyTest::testWithRedirect()
{
	QFETCH( QUrl, redirectTarget );
	QFETCH( int, statusCode );

	MockReplyBuilder builder;
	builder.withRedirect( redirectTarget, static_cast<HttpStatus::Code>( statusCode ) );

	MockReplyUniquePtr reply( builder.createReply() );

	const QUrl locationHeader = QUrl::fromEncoded( reply->rawHeader( HttpUtils::locationHeader() ), QUrl::StrictMode );
	QCOMPARE( locationHeader, redirectTarget );
	QCOMPARE( reply->attribute( QNetworkRequest::HttpStatusCodeAttribute ).toInt(), statusCode );

	const QUrl reqTarget( "http://foo.bar/test" );
	MockReplyTestHelper::callPrepare( reply.get(), Request( QNetworkRequest( reqTarget ) ) );
	QCOMPARE( locationHeader, redirectTarget );
	QCOMPARE( reply->attribute( QNetworkRequest::RedirectionTargetAttribute ).toUrl(), redirectTarget );

}

/*! Provides the data for the testWithRedirect() test.
 */
void MockReplyTest::testWithRedirect_data()
{
	QTest::addColumn<QUrl>( "redirectTarget" );
	QTest::addColumn<int>( "statusCode" );

	//                                    // redirectTarget               // statusCode
	QTest::newRow( "relative url" )       << QUrl( "see/other" )          << static_cast<int>( HttpStatus::MovedPermanently );
	QTest::newRow( "absolute url" )       << QUrl( "http://example.com" ) << static_cast<int>( HttpStatus::MovedPermanently );
	QTest::newRow( "temporary redirect" ) << QUrl( "http://example.com" ) << static_cast<int>( HttpStatus::TemporaryRedirect );
}

/*! \test Tests the MockReplyBuilder::withHeader() method.
 */
void MockReplyTest::testWithHeader()
{
	QFETCH( QNetworkRequest::KnownHeaders, header );
	QFETCH( QVariant, value );

	MockReplyBuilder builder;
	builder.withHeader( header, value );

	MockReplyUniquePtr reply( builder.createReply() );

	QCOMPARE( reply->header( header ), value );
}

/*! Provides the data for the testWithHeader() test.
 */
void MockReplyTest::testWithHeader_data()
{
	QTest::addColumn<QNetworkRequest::KnownHeaders>( "header" );
	QTest::addColumn<QVariant>( "value" );

	QTest::newRow( "content type" )      << QNetworkRequest::ContentTypeHeader   << QVariant( "text/plain" );
	QTest::newRow( "content length" )    << QNetworkRequest::ContentLengthHeader << QVariant::fromValue( 42 );
	QTest::newRow( "null value" )        << QNetworkRequest::LocationHeader      << QVariant();
	QTest::newRow( "relative location" ) << QNetworkRequest::LocationHeader      << QVariant( QUrl( "//example.com" ) );
}

/*! Provides the data for the testWithRawHeader() test.
 */
void MockReplyTest::testWithRawHeader_data()
{
	QTest::addColumn<QByteArray>("header");
	QTest::addColumn<QByteArray>("value");

	QTest::newRow("content type")    << QByteArray("Content-Type") << QByteArray("text/plain");
	QTest::newRow("case is ignored") << QByteArray("conTent-Type") << QByteArray("text/plain");
	QTest::newRow("custom header")   << QByteArray("X-Foo") << QByteArray("17");
	QTest::newRow("null value")      << QByteArray("Content-Length") << QByteArray();
	QTest::newRow("empty value")     << QByteArray("Location") << QByteArray("");
}

/*! \test Tests the MockReplyBuilder::withRawHeader() method.
 */
void MockReplyTest::testWithRawHeader()
{
	QFETCH(QByteArray, header);
	QFETCH(QByteArray, value);

	MockReplyBuilder builder;
	builder.withRawHeader(header, value);

	MockReplyUniquePtr reply(builder.createReply());

	QCOMPARE(reply->rawHeader(header), value);
}

/*! Provides the data for the testWithAttribute() test.
 */
void MockReplyTest::testWithAttribute_data()
{
	QTest::addColumn<QNetworkRequest::Attribute>("attribute");
	QTest::addColumn<QVariant>("value");

	QTest::newRow("status code") << QNetworkRequest::HttpStatusCodeAttribute   << QVariant::fromValue(200);
	QTest::newRow("null value")  << QNetworkRequest::HttpReasonPhraseAttribute << QVariant();
	QTest::newRow("empty value") << QNetworkRequest::HttpReasonPhraseAttribute << QVariant("");
}

/*! \test Tests the MockReplyBuilder::withAttribute() method.
 */
void MockReplyTest::testWithAttribute()
{
	QFETCH(QNetworkRequest::Attribute, attribute);
	QFETCH(QVariant, value);

	MockReplyBuilder builder;
	builder.withAttribute(attribute, value);

	MockReplyUniquePtr reply(builder.createReply());

	QCOMPARE(reply->attribute(attribute), value);
}


/*! \test Tests the MockReplyBuilder::withAuthenticate() method.
 */
void MockReplyTest::testWithAuthenticate()
{
	QFETCH( QString, realm );

	MockReplyBuilder builder;

	if ( QTest::currentDataTag() == QString( "invalid challenge" ) )
		Tests::ignoreMessage( QtWarningMsg, "Invalid authentication header: Missing value for parameter: \"realm\"" );

	builder.withAuthenticate( realm );

	MockReplyUniquePtr reply( builder.createReply() );

	QTEST( reply->attribute( QNetworkRequest::HttpStatusCodeAttribute ).toInt(), "statusCode" );
	QTEST( reply->rawHeader( "WWW-Authenticate" ), "authenticateHeader" );
}

/*! Provides the data for the testWithAuthenticate() test.
 */
void MockReplyTest::testWithAuthenticate_data()
{
	QTest::addColumn< QString >( "realm" );
	QTest::addColumn< int >( "statusCode" );
	QTest::addColumn< QByteArray >( "authenticateHeader" );

	QTest::newRow( "basic auth" )        << "world"   << static_cast< int >( HttpStatus::Unauthorized ) << QByteArray( "Basic realm=\"world\"" );
	QTest::newRow( "invalid challenge" ) << QString() << 0                                              << QByteArray();
}


/*! Provides the data for the testWithCookie() test.
 */
void MockReplyTest::testWithCookie_data()
{
	QTest::addColumn<QList<QNetworkCookie> >("cookies");

	const QByteArray cookieName("foo");

	QNetworkCookie sessionCookie(cookieName, "bar");
	sessionCookie.setPath("/foo");
	sessionCookie.setDomain(".example.com");

	QNetworkCookie expiringCookie("expiring", "tick tack");
	expiringCookie.setExpirationDate(QDateTime::currentDateTime().addDays(2));

	QNetworkCookie expiredCookie("expired", "boom");
	expiredCookie.setExpirationDate(QDateTime::currentDateTime().addSecs(-10));

	QNetworkCookie overridingCookie(cookieName, "overridden");
	overridingCookie.setPath("/foo");
	overridingCookie.setDomain(".example.com");


	//                                  // cookies
	QTest::newRow("session cookie")     << (QList<QNetworkCookie>() << sessionCookie);
	QTest::newRow("expiring cookie")    << (QList<QNetworkCookie>() << expiringCookie);
	QTest::newRow("expired cookie")     << (QList<QNetworkCookie>() << expiredCookie);
	QTest::newRow("multiple cookies")   << (QList<QNetworkCookie>() << sessionCookie << expiringCookie);
	QTest::newRow("overriding cookies") << (QList<QNetworkCookie>() << sessionCookie << overridingCookie);
}

/*! \test Tests the MockReplyBuilder::withCookie() method.
 */
void MockReplyTest::testWithCookie()
{
	QFETCH(QList<QNetworkCookie>, cookies);

	MockReplyBuilder builder;
	QList<QNetworkCookie>::Iterator iter;
	for (iter = cookies.begin(); iter != cookies.end(); ++iter)
		builder.withCookie(*iter);

	MockReplyUniquePtr reply(builder.createReply());
	const QList<QNetworkCookie> actualCookies = reply->header(QNetworkRequest::SetCookieHeader).value<QList<QNetworkCookie> >();

	QCOMPARE(actualCookies, cookies);
}

/*! \test Tests the MockReplyBuilder::withFinishDelayUntil() method.
 */
void MockReplyTest::testWithFinishDelayUntil()
{
	DummyObject helper;

	MockReplyBuilder builder;
	builder.withStatus( HttpStatus::OK ).withFinishDelayUntil( &helper, "dummySignal()" );

	MockReplyUniquePtr reply( builder.createReply() );

	Request request( QNetworkRequest( QUrl( "http://example.com" ) ) );

	QSignalSpy finishedSpy( reply.get(), SIGNAL(finished()) );
	QSignalSpy metaDataChangedSpy( reply.get(), SIGNAL(metaDataChanged()) );
	MockReplyTestHelper::callFinish( reply.get(), request );

	QTest::qWait( 10 );

	QVERIFY( finishedSpy.isEmpty() );
	QVERIFY( !reply->isFinished() );
	QCOMPARE( metaDataChangedSpy.size(), 1 );

	helper.dummySignal();

	QTest::qWait( 1 );

	QCOMPARE( finishedSpy.size(), 1 );
	QVERIFY( reply->isFinished() );
}

void MockReplyTest::testWithFinishDelayUntilMemberPointer()
{
#if ( __cplusplus >= 201103L || ( defined( _MSC_VER ) && _MSC_VER >= 1900 ) )

	DummyObject helper;

	MockReplyBuilder builder;
	builder.withFinishDelayUntil( &helper, "dummySignal()" );

	MockReplyBuilder builderWithMemberPointer;
	builderWithMemberPointer.withFinishDelayUntil( &helper, &DummyObject::dummySignal );

	QCOMPARE( builderWithMemberPointer, builder );

#endif // C++ >= 11
}


/*! \test Tests the MockReplyBuilder::operator==() and MockReplyBuilder::operator!=().
 */
void MockReplyTest::testEqualityOperators()
{
	QFETCH( MockReplyBuilderPtr, left );
	QFETCH( MockReplyBuilderPtr, right );
	QFETCH( bool, equal );

	QVERIFY( *left  == *left );
	QVERIFY( *right == *right );
	QCOMPARE( *left  == *right, equal );
	QCOMPARE( *right == *left,  equal );
	QCOMPARE( *left  != *right, !equal );
	QCOMPARE( *right != *left,  !equal );
}

/*! Provides the data for the testOperatorEquals() test.
 */
void MockReplyTest::testEqualityOperators_data()
{
	QTest::addColumn<MockReplyBuilderPtr>("left");
	QTest::addColumn<MockReplyBuilderPtr>("right");
	QTest::addColumn<bool>("equal");

	MockReplyBuilderPtr nullBuilder(new MockReplyBuilder);
	MockReplyBuilderPtr nullBuilder2(new MockReplyBuilder);
	MockReplyBuilderPtr simpleBuilder(new MockReplyBuilder);
	simpleBuilder->withStatus(HttpStatus::OK);
	MockReplyBuilderPtr simpleBuilder2(new MockReplyBuilder);
	simpleBuilder2->withStatus(HttpStatus::OK);
	MockReplyBuilderPtr differentSimpleBuilder(new MockReplyBuilder);
	differentSimpleBuilder->withStatus(HttpStatus::Created);

	MockReplyBuilderPtr fullBuilder = HelperMethods::createFullBuilder();
	MockReplyBuilderPtr fullBuilder2 = HelperMethods::createFullBuilder();
	MockReplyBuilderPtr differentBodyBuilder = HelperMethods::createFullBuilder();
	differentBodyBuilder->withBody("This is different");
	MockReplyBuilderPtr differentHeaderBuilder = HelperMethods::createFullBuilder();
	differentHeaderBuilder->withHeader(QNetworkRequest::ContentTypeHeader, QByteArray("something/different"));
	MockReplyBuilderPtr additionalHeaderBuilder = HelperMethods::createFullBuilder();
	additionalHeaderBuilder->withHeader(QNetworkRequest::ServerHeader, QByteArray("TestServer"));
	MockReplyBuilderPtr differentRawHeaderBuilder = HelperMethods::createFullBuilder();
	differentRawHeaderBuilder->withRawHeader(customHeader, "different");
	MockReplyBuilderPtr additionalRawHeaderBuilder = HelperMethods::createFullBuilder();
	additionalRawHeaderBuilder->withRawHeader("X-X", "o-o");
	MockReplyBuilderPtr differentAttributeBuilder = HelperMethods::createFullBuilder();
	differentAttributeBuilder->withAttribute(QNetworkRequest::HttpReasonPhraseAttribute, QByteArray("Created"));
	MockReplyBuilderPtr additionalAttributeBuilder = HelperMethods::createFullBuilder();
	additionalAttributeBuilder->withAttribute(QNetworkRequest::RedirectionTargetAttribute, QUrl("http://example.com"));

	MockReplyBuilderPtr errorBuilder = HelperMethods::createErrorBuilder();
	MockReplyBuilderPtr errorBuilder2 = HelperMethods::createErrorBuilder();
	MockReplyBuilderPtr differentErrorCodeBuilder = HelperMethods::createErrorBuilder();
	differentErrorCodeBuilder->withError(QNetworkReply::ContentAccessDenied, "Content resend");
	MockReplyBuilderPtr differentErrorMessageBuilder = HelperMethods::createErrorBuilder();
	differentErrorMessageBuilder->withError(QNetworkReply::ContentReSendError, "Some other error message");


	//                                         // left                   // right                        // equal
	QTest::newRow("equal simple builders")     << simpleBuilder          << simpleBuilder2               << true;
	QTest::newRow("equal full builders")       << fullBuilder            << fullBuilder2                 << true;
	QTest::newRow("equal error builders")      << errorBuilder           << errorBuilder2                << true;
	QTest::newRow("two null")                  << nullBuilder            << nullBuilder2                 << true;
	QTest::newRow("different simple builders") << simpleBuilder          << differentSimpleBuilder       << false;
	QTest::newRow("different body")            << fullBuilder            << differentBodyBuilder         << false;
	QTest::newRow("different header")          << fullBuilder            << differentHeaderBuilder       << false;
	QTest::newRow("additional header")         << fullBuilder            << additionalHeaderBuilder      << false;
	QTest::newRow("different raw header")      << fullBuilder            << differentRawHeaderBuilder    << false;
	QTest::newRow("additional raw header")     << fullBuilder            << additionalRawHeaderBuilder   << false;
	QTest::newRow("different attribute")       << fullBuilder            << differentAttributeBuilder    << false;
	QTest::newRow("additional attribute")      << fullBuilder            << additionalAttributeBuilder   << false;
	QTest::newRow("different error code")      << errorBuilder           << differentErrorCodeBuilder    << false;
	QTest::newRow("different error message")   << errorBuilder           << differentErrorMessageBuilder << false;
	QTest::newRow("left null")                 << nullBuilder            << simpleBuilder                << false;
	QTest::newRow("right null")                << simpleBuilder          << nullBuilder                  << false;

	// TODO: Compare defined error string vs. default error string

}

void MockReplyTest::testErrorAndStatusCodeConsistencyWarning()
{
	QFETCH( QNetworkReply::NetworkError, error );
	QFETCH( HttpStatus::Code, statusCode );
	QFETCH( bool, expectWarning );

	const QRegularExpression expectedWarning( "HTTP status code and QNetworkReply::error\\(\\) do not match!.*" );

	MockReplyBuilder errorFirstBuilder;
	errorFirstBuilder.withError( error );

	if( expectWarning )
		Tests::ignoreMessage( QtWarningMsg, expectedWarning );
	errorFirstBuilder.withStatus( statusCode );

	MockReplyBuilder statusFirstBuilder;
	statusFirstBuilder.withStatus( statusCode );

	if( expectWarning )
		Tests::ignoreMessage( QtWarningMsg, expectedWarning );
	statusFirstBuilder.withError( error );
}

void MockReplyTest::testErrorAndStatusCodeConsistencyWarning_data()
{
	QTest::addColumn< QNetworkReply::NetworkError >( "error" );
	QTest::addColumn< HttpStatus::Code >( "statusCode" );
	QTest::addColumn< bool >( "expectWarning" );

	QTest::newRow( "no error with client error code" )                         << QNetworkReply::NoError              << HttpStatus::BadRequest << true;
	QTest::newRow( "client error with mismatching client error code" )         << QNetworkReply::ContentNotFoundError << HttpStatus::BadRequest << true;
	QTest::newRow( "client error with matching client error code" )            << QNetworkReply::ContentNotFoundError << HttpStatus::NotFound   << false;
	QTest::newRow( "protocol error with client error code" )                   << QNetworkReply::ProtocolFailure      << HttpStatus::BadRequest << true;
	QTest::newRow( "unknown client error with matching client error code" )    << QNetworkReply::UnknownContentError  << HttpStatus::Locked     << false;
	QTest::newRow( "unknown client error with mismatching client error code" ) << QNetworkReply::UnknownContentError  << HttpStatus::Forbidden  << true;
	#if QT_VERSION >= QT_VERSION_CHECK( 5,3,0 )
		QTest::newRow( "unknown server error with unknown client error code" ) << QNetworkReply::UnknownServerError   << HttpStatus::Locked  << true;
	#endif // Qt >= 5.3.0
}

MockReplyBuilderPtr HelperMethods::createFullBuilder()
{
	MockReplyBuilderPtr fullBuilder(new MockReplyBuilder);
	fullBuilder->withStatus(HttpStatus::Created);
	fullBuilder->withBody("foo bar");
	fullBuilder->withHeader(QNetworkRequest::ContentTypeHeader, QByteArray("foo/bar"));
	fullBuilder->withRawHeader(customHeader, "woot");
	fullBuilder->withAttribute(QNetworkRequest::HttpReasonPhraseAttribute, QByteArray("Spawned"));
	return fullBuilder;
}

MockReplyBuilderPtr HelperMethods::createErrorBuilder()
{
	MockReplyBuilderPtr errorBuilder(new MockReplyBuilder);
	errorBuilder->withError(QNetworkReply::ContentReSendError, "Content resend");
	errorBuilder->withBody("Error");
	return errorBuilder;
}

MockReplyBuilder HelperMethods::createErrorBuilder( QNetworkReply::NetworkError error )
{
	MockReplyBuilder errorBuilder;
	errorBuilder.withError( error );
	return errorBuilder;
}

HeaderHash HelperMethods::getKnownHeaders(QNetworkReply* reply)
{
	HeaderHash result;
	KnownHeadersSet headers;
	headers << QNetworkRequest::ContentTypeHeader
	        << QNetworkRequest::ContentLengthHeader
	        << QNetworkRequest::LocationHeader
	        << QNetworkRequest::LastModifiedHeader
	        << QNetworkRequest::CookieHeader
	        << QNetworkRequest::SetCookieHeader
	        << QNetworkRequest::ContentDispositionHeader
	        << QNetworkRequest::UserAgentHeader
	        << QNetworkRequest::ServerHeader;
#if QT_VERSION >= QT_VERSION_CHECK(5,12,0)
	headers << QNetworkRequest::IfModifiedSinceHeader
	        << QNetworkRequest::ETagHeader
	        << QNetworkRequest::IfMatchHeader
	        << QNetworkRequest::IfNoneMatchHeader;
#endif // Qt >= 5.12.0

	for (KnownHeadersSet::const_iterator iter = headers.cbegin(); iter != headers.cend(); ++iter)
		result.insert(*iter, reply->header(*iter));

	return result;
}

OptionalRegEx makeOptionalRegEx( const QString& pattern, QRegularExpression::PatternOptions options )
{
	return qMakePair( true, QRegularExpression( pattern, options ) );
}



} // namespace Tests

QTEST_MAIN(Tests::MockReplyTest)
#include "MockReplyTest.moc"
