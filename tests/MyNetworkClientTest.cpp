/*! \file
 */
#include "MyNetworkClient.hpp"

#include "MockNetworkAccessManager.hpp"
#include <QtTest>

namespace Tests
{

/*! This is an example class demonstrating how to setup a MockNetworkAccess::Manager,
 * inject it into a class under test and verify expectations.
 *
 * The \ref MyNetworkClientTest.cpp "source code" is included as an example in this documentation.
 */
class MyNetworkClientTest : public QObject
{
	Q_OBJECT

private slots:
	void testFetchHello()
	{
		// Create and configure the mocked network access manager
		MockNetworkAccess::Manager< QNetworkAccessManager > mnam;

		mnam.whenGet( QUrl( "http://example.com/hello" ) )
		    .has( MockNetworkAccess::Predicates::HeaderMatching( QNetworkRequest::UserAgentHeader,
		                                                          QRegularExpression( ".*MyNetworkClient/.*" ) ) )
		    .reply().withBody( QJsonDocument::fromJson( "{\"hello\":\"world\"}" ) );

		// Call the method under test
		MyNetworkClient client;
		client.setNetworkAccessManager( &mnam );
		client.fetchHello();

		QTest::qWait( 1 ); /* The signals of the QNetworkReply are emitted asynchronously (Qt::QueuedConnection).
		                    * So we need to process events to have the client react to the signals.
		                    */

		// Verify expectations
		QCOMPARE( mnam.matchedRequests().length(), 1 );
		QCOMPARE( client.hello(), QString( "world" ) );
	}
};

} // namespace Tests

QTEST_MAIN( Tests::MyNetworkClientTest )
#include "MyNetworkClientTest.moc"
