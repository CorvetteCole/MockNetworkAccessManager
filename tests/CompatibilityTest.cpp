#include "MockNetworkAccessManager.hpp"
#include <QNetworkAccessManager>
#include <QObject>
#include <QTest>

namespace Tests
{

/*! Tests compatibility of the MockNetworkAccessManager with constrained projects.
 *
 * This test is more like a compile test rather than a real unit tests.
 * It checks if the MockNetworkAccessManager also compiles in projects that
 * use constraining compile settings like QT_NO_CAST_TO_ASCII etc.
 */
class CompatibilityTest : public QObject
{
	Q_OBJECT

private Q_SLOTS:
	void test();
};

/*! \test Dummy test which ensures that a Manager is instantiated.
 */
void CompatibilityTest::test()
{
	MockNetworkAccess::Manager<QNetworkAccessManager> mnam;

	QVERIFY( mnam.receivedRequests().isEmpty() );
}

} // namespace Tests


QTEST_MAIN( Tests::CompatibilityTest )
#include "CompatibilityTest.moc"
