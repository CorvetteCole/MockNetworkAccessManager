import qbs
import qbs.FileInfo

QtApplication {
	type: base.concat( [ "autotest" ] )

	Depends { name: "Qt"
		submodules: [
			"core",
			"network",
			"test",
		]
	}

	Depends { name: "MockNetworkAccessManager" }

	cpp.includePaths: [ ".." ]
	cpp.systemIncludePaths: {
		var sysIncludePaths = [ Qt.core.incPath ];
		if( Qt.core.frameworkBuild ) {
			[ 'Core', 'Network', 'Test' ].forEach( function( lib ) {
				sysIncludePaths.push( FileInfo.joinPaths( Qt.core.libPath, 'Qt' + lib + '.framework', 'Headers' ) );
			} );
		}
		return sysIncludePaths;
	}
	cpp.defines: [ "QT_DEPRECATED_WARNINGS" ]
	cpp.cxxLanguageVersion: [ 'c++11' ]
	cpp.treatWarningsAsErrors: true

	cpp.rpaths: qbs.targetOS.contains( "unix" ) ? [ Qt.core.libPath ] : undefined

	files: [
		name + ".cpp"
	]
}