import qbs

Project {

	property bool enableTests: false

	StaticLibrary {
		name: "MockNetworkAccessManager"
		version: "0.10.1"

		Depends { name: "Qt"
			submodules: [
				"core",
				"network",
			]
		}

		Export {
			Depends { name: "cpp" }
			Depends { name: "Qt"
				submodules: [
					"core",
					"network",
				]
			}

			cpp.includePaths: [ product.sourceDirectory ]
		}

		cpp.defines: [ "QT_DEPRECATED_WARNINGS" ]
		cpp.cxxLanguageVersion: [ 'c++11' ]
		cpp.treatWarningsAsErrors: true

		files: [
			"MockNetworkAccessManager.hpp"
		]

	}

	references: enableTests ? [ "tests/tests_project.qbs" ] : []
}