#include "MockNetworkAccessManager.hpp"
#include <QNetworkAccessManager>

int main(int argc, char *argv[])
{
	//! [main]

	using namespace MockNetworkAccess;
	using namespace MockNetworkAccess::Predicates;

	Manager< QNetworkAccessManager > fallbackMockNam;
	fallbackMockNam.setUnmatchedRequestBuilder(
		MockReplyBuilder().withStatus( HttpStatus::BadRequest )
		                  .withBody( "Unexpected custom request" ) );
	fallbackMockNam.setUnmatchedRequestBehavior( PredefinedReply );

	Rule unexpectedGet;
	unexpectedGet.has( Verb( QNetworkAccessManager::GetOperation ) )
	             .reply().withStatus( HttpStatus::BadRequest )
	                     .withBody( "Unexpected GET request" );
	Rule unexpectedPost;
	unexpectedPost.has( Verb( QNetworkAccessManager::PostOperation ) )
	              .reply().withStatus( HttpStatus::BadRequest )
	                      .withBody( "Unexpected POST request" );
	Rule unexpectedPut;
	unexpectedPost.has( Verb( QNetworkAccessManager::PutOperation ) )
	              .reply().withStatus( HttpStatus::BadRequest )
	                      .withBody( "Unexpected PUT request" );
	Rule unexpectedDelete;
	unexpectedPost.has( Verb( QNetworkAccessManager::DeleteOperation ) )
	              .reply().withStatus( HttpStatus::BadRequest )
	                      .withBody( "Unexpected DELETE request" );

	fallbackMockNam.addRule( unexpectedGet );
	fallbackMockNam.addRule( unexpectedPost );
	fallbackMockNam.addRule( unexpectedPut );
	fallbackMockNam.addRule( unexpectedDelete );


	Manager< QNetworkAccessManager > mockNam;
	mockNam.setPassThroughNam( &fallbackMockNam );
	mockNam.setUnmatchedRequestBehavior( PassThrough );

	mockNam.whenGet( QUrl( "http://example.com" ) )
	       .reply().withBody( "hello world!" );

	//! [main]

	return 0;
}
