﻿namespace MockNetworkAccess {

/*!

\page page_passThrough Guide: Passing Requests Through

\brief This page describes how requests can be passed through to the network instead of being answered with mocked replies.

There can be situations where (certain) requests should not be answered with mocked replies but with real replies from the server.

The MockNetworkAccessManager provides two ways to handle this:
- Pass specific requests through by setting Rule::passThrough(). Then, all requests matching the given Rule will be passed through.
- Pass through all requests that do not match any rule by setting Manager::setUnmatchedRequestBehavior() to PassThrough.

In both cases, the requests are passed through to the Manager's pass through network access manager.
By default, this is the \p Base class of the Manager unless it was overridden with Manager::setPassThroughNam().


\section page_passThrough_mockHierarchy Hierarchy of MockNetworkAccess::Managers

It is possible to build up a hierarchy of MockNetworkAccess::Managers by setting a MockNetworkAccess::Manager as pass through NAM
(see Manager::setPassThroughNam() and Rule::passThrough()).

This allows, for example, to handle unexpected requests differently based on their type:

\snippet mnamHierarchy.cpp main


\section page_passThrough_redirects Pass Through & Redirects

At the moment, when passing through requests that have automatic redirect following enabled, the redirects are handled by the pass through network access manager. This means that the rules of the initial network access manager are not applied to the redirects.

So if a request with automatic redirect following gets passed through to a real QNetworkAccessManager and the request is redirected to a URL that would normally return a mocked reply, the redirect is handled by the real QNetworkAccessManager anyway and therefore goes to the network.

However, there is a feature request to remove this limitation (see issue \issue{15}).

*/

} // namespace MockNetworkAccess
