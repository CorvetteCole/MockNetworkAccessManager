import gitlab
import os
import argparse
from pathlib import Path


def main():
	args = parseCommandLine()
	instance = gitlab.Gitlab( os.environ['CI_SERVER_URL'], private_token=os.environ['API_TOKEN'] )
	project = instance.projects.get( int( os.environ['CI_PROJECT_ID'] ) )
	milestone = getMilestone( project )

	createRelease( project, milestone, args )
	closeMilestone( milestone )


def parseCommandLine():
	argParser = argparse.ArgumentParser( description='Create a release out of a GitLab CI pipeline' )

	argParser.add_argument( '--description-file', type=Path,
	                         help='Path to the file containing the description of the release.' )

	return argParser.parse_args()


def getMilestone( project ):
	tag = os.environ[ 'CI_COMMIT_TAG' ]
	matchingMilestones = project.milestones.list( search=tag )
	for milestone in matchingMilestones:
		if milestone.title == tag:
			return milestone
	return None


def createRelease( project, milestone, args ):
	releaseProperties = getReleaseProperties( args, milestone )
	project.releases.create( releaseProperties )


def getReleaseProperties( args, milestone ):
	props = {}
	tag = os.environ[ 'CI_COMMIT_TAG' ]
	props[ 'name' ] = tag
	props[ 'tag_name' ] = tag
	if args.description_file:
		props[ 'description' ] = args.description_file.read_text( encoding='utf-8-sig' )
	if milestone:
		props[ 'milestones' ] = [ milestone.title ]
	return props


def closeMilestone( milestone ):
	milestone.state_event = 'close'
	milestone.save()


if __name__ == '__main__':
	main()
